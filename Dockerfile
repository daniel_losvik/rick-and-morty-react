
FROM node:lts-alpine AS Builder

ENV PORT 3000

WORKDIR /src
COPY package.json .
RUN npm install    
COPY . .

RUN npm run build

FROM node:lts-alpine AS Production

ENV PORT 3000

WORKDIR /app
COPY package.json .
RUN npm install express compression
COPY index.js .
COPY --from=builder /src/build ./build

EXPOSE 3000/tcp


CMD [ "npm", "start" ]    


