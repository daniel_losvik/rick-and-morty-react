import React from '../../../node_modules/react';
import "./CharacterCard.css"
import { useHistory } from "react-router-dom";


const CharacterCard = function(props) {
    const history = useHistory();

    function navigateToCharacterDetailPage() {
        history.push("/characters/" + props.character.id);
    } 
    
    return (
        <div className="character-card-container" onClick={(e) => {navigateToCharacterDetailPage(e)}} >
            <div className="character-image">
                <img src={props.character.image} alt="character" />
                <div className="name-container">{props.character.name}</div>
            </div>
            <div className="text-container">
                <div>Status</div>
                <div>{props.character.status}</div>
            </div>
            <hr/>
            <div className="text-container">
                <div>Species</div>
                <div>{props.character.species}</div>
            </div>
            <hr/>
            <div className="text-container">
                <div>Gender</div>
                <div>{props.character.gender}</div>
            </div>
            <hr/>
            <div className="text-container">
                <div>Origin</div>
                <div>{props.character.origin.name}</div>
            </div>
            <hr/>
            <div className="text-container">
                <div>Last location</div>
                <div>{props.character.location.name}</div>
            </div>
            <div className="view-profile-link">
                View Profile
            </div>
        </div>
    );
}

export default CharacterCard;