import React from 'react';
import './App.css';
import CharacterList from "./containers/CharacterList/CharacterList.js"
import CharacterDetails from "./containers/CharacterDetails/CharacterDetails.js"
import {
  BrowserRouter,
  Switch,
  Route,
  Link,
} from "react-router-dom";

function App() {
  return (
    <div className="App">
        <BrowserRouter>
          <Link to="/">
            <div className="header">
            </div>
          </Link>
          <Switch>
            <Route exact path="/">
              <CharacterList />
            </Route>
            <Route exact path="/characters/:id">
              <CharacterDetails />
            </Route>
          </Switch>
        </BrowserRouter>
    </div>
  );
}

export default App;
