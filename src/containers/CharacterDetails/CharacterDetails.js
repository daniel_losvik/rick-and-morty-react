import React from '../../../node_modules/react';
import "./CharacterDetails.css"
import { withRouter } from "react-router";


class CharacterDetails extends React.Component {
    constructor() {
        super()
        this.state = {
            character: null,
        };
    }
    render() {
        if(this.state.character) {
            return (
                <div className="character-detail-container-background">
                    <div className="character-detail-container">
                        <div className="image-and-name-container-character-detail">
                            <div >
                                <img className="character-image-character-detail" src={this.state.character.image} alt="character" />
                            </div>
                            <div className="name-container-character-detail">
                                {this.state.character.name}
                                <br/>
                                <div className="status-container-character-detail">
                                    Status: {this.state.character.status}
                                </div>
                            </div>
                        </div>
                        <div className="profile-container-character-detail">
                            <div className="profile-text">
                            Profile
                            </div>
                            <div className="text-container-character-detail">
                                <div>Gender</div>
                                <div>{this.state.character.gender}</div>
                            </div>
                            <hr/>
                            <div className="text-container-character-detail">
                                <div>Species</div>
                                <div>{this.state.character.species}</div>
                            </div>
                            <hr/>
                            <div className="text-container-character-detail">
                                <div>Origin</div>
                                <div>{this.state.character.origin.name}</div>
                            </div>
                            <hr/>
                        </div>
                        <div className="location-container-character-detail">
                            <div className="location-text">
                            Location
                            </div>
                            <div className="text-container-character-detail">
                                <div>Last location</div>
                                <div>{this.state.character.location.name}</div>
                            </div>
                            <hr/>
                            <div className="text-container-character-detail">
                                <div>Type</div>
                                <div>{this.state.character.location.type}</div>
                            </div>
                            <hr/>
                            <div className="text-container-character-detail">
                                <div>Dimension</div>
                                <div>{this.state.character.location.dimension}</div>
                            </div>
                            <hr/>
                            <br/>
                        </div>
                    </div>
                </div>
            )
        }
        else {
            return (
            <div className="character-detail-container">
            </div>
            );
        }
    }
    componentDidMount() {
        fetch("https://rickandmortyapi.com/api/character/" + this.props.match.params.id)
        .then(response => {return response.json()})
        .then(data => {

            fetch(data.location.url)
            .then(response_2 => {return response_2.json()})
            .then(data_2 => {
                data.location =data_2
                this.setState({ 
                    "character": data
                })
            })
        })
    }
}

export default withRouter(CharacterDetails);