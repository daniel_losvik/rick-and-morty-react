import React from '../../../node_modules/react';
import "./CharacterList.css"
import CharacterCard from "../../components/CharacterCard/CharacterCard.js"


class CharacterList extends React.Component {
    constructor() {
        super()
        this.state = {
            characters: [],
            filteredCharacters: [],
            URLToGetMoreCharacters: null,
            searchString: "",
        };
    }
    render() {
        return (
            <div>
                <div className="character-list-container">
                    <input 
                        type="text" 
                        name="searchKey" 
                        className="search-field" 
                        placeholder="Enter a name.." 
                        value={this.state.searchKey}
                        onChange={(e) => {this.filterCharacters(e.target.value)}} />
                    {this.state.filteredCharacters}
                </div>
                <div className="load-more-button" onClick={(e) => {this.fetchMoreCharacters(e.target.value)}}>
                    <button>Load More</button>
                </div>
            </div>
          );
    }
    componentDidMount() {
        fetch("https://rickandmortyapi.com/api/character/")
        .then(response => {return response.json()})
        .then(data => {
            let characters = []
            data.results.forEach((character, index) => {
                characters.push(<li key={index}><CharacterCard character={character}/></li>)
            });
            this.setState({ 
                "characters": characters,
                "URLToGetMoreCharacters": data.info.next
            })
            this.filterCharacters(this.state.searchString)
        })
    }
    fetchMoreCharacters() {
        if(this.state.URLToGetMoreCharacters) {
            fetch(this.state.URLToGetMoreCharacters)
            .then(response => {return response.json()})
            .then(data => {
                let characters = this.state.characters
                data.results.forEach(character => {
                    characters.push(<li key={character.id}><CharacterCard character={character}/></li>)
                });
                this.setState({ 
                    "characters": characters,
                    "URLToGetMoreCharacters": data.info.next
                })
                this.filterCharacters(this.state.searchString)
            })
            }
        }
    filterCharacters(userInput) {
        this.setState({
            "searchString": userInput,
            "filteredCharacters": this.state.characters.filter(character =>{
            return character.props.children.props.character.name.toUpperCase().includes(userInput.toUpperCase())
        })})
      }
}

export default CharacterList;